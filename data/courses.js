export default [ //this will serve as the module that holds the records about the courses/ subjects in our booking system.
  {
     id:  "wdc001",                   //string
     name: "50000",                  //string
     description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Autem deserunt beatae, laborum fugit possimus mollitia corporis, aliquid ipsam voluptate quo, adipisci sed incidunt voluptas amet aperiam eum alias veniam animi.",        //string
     price: 45000,
     instructor: "Mr Kung",                            //num
     onOffer: true //this will describe if the subject is available for enrollments.
  },
   {
     id: "wdc002",
     name: "Python - Django",
     description: "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam, eum distinctio ipsam suscipit aliquid cumque, numquam ipsum dignissimos. Deserunt nam reiciendis quod aperiam repellendus inventore repudiandae autem praesentium magni, in.",
     price: 55000,
     instructor: "Mr Fu",      
     onOffer: true //this will decribe if the subject is available for enrollments.
  },
   {
     id: "wdc003",
     name: "Java - Springboot",
     description: "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Similique pariatur et nihil sequi aliquam fuga amet odio doloremque rem nobis facere, maiores at corrupti magnam! Sit est ipsam recusandae earum.",
     price: 45000,
     instructor: "Mr Panda",      
     onOffer: true //this will decribe if the subject is available for enrollments.
  },
  {
     id: "wdc004",
     name: "Javascript",
     description: "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Similique pariatur et nihil sequi aliquam fuga amet odio doloremque rem nobis facere, maiores at corrupti magnam! Sit est ipsam recusandae earum.",
     price: 125000,
      instructor: "Mr Fu",  
     onOffer: true //this will decribe if the subject is available for enrollments.
  },
   {
     id: "wdc005",
     name: "React JS",
     description: "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Similique pariatur et nihil sequi aliquam fuga amet odio doloremque rem nobis facere, maiores at corrupti magnam! Sit est ipsam recusandae earum.",
     price: 300000,
      instructor: "Mr Fu",  
     onOffer: true //this will decribe if the subject is available for enrollments.
  }
]
