import { createContext } from 'react'
//our new task is to create a context data for our app which will describe the state of our user.

//1. lets use the constructor to create a context object
const UserContext = createContext();

export default UserContext;

//2. lets acquire its provider component property which will allow the consumer components to subscribe to context changes to values it holds/contains.
export const UserProvider = UserContext.Provider;