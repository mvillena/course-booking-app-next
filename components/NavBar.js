// import { useContext } from 'react'
// import UserContext from '../UserContext'; 
import { useState, useContext } from 'react'
import { Navbar, Container, Nav } from 'react-bootstrap';
//lets acquire The routing components of next JS
import Link from 'next/link'

//acquire the context object that determines the value of the user
import UserContext from '../UserContext';

export default function NavBar() {
    const [isExpanded, setIsExpanded] = useState(false)

    //destructure the context object and acquire the values/components you want to consume.
    const { user } = useContext(UserContext)

    //create a ternary structure for us to be able to determine what element can be seen if user is mounted.
 
    return(
   	    // lets render the navbar component as routing components
		<Navbar expanded={isExpanded} expand="lg" variant="dark" bg="dark" fixed="top">
			  <Container>
			     <Link href="/">
			     	<a className="navbar-brand">Batch 99 CLUB!</a>
			     </Link>
			     <Navbar.Toggle aria-controls="basic-navbar-nav"/>
				 <Navbar.Collapse id="basic-navbar-nav">
				    <Nav className="mr-auto">
				  
				      	<>
				      	 { (user.email) 
				      	 	? 
				      	  <>
					      	  <Link href="/courses">
				     			<a className="nav-link">Courses</a>
				              </Link>
				          	 
					      	  <Link href="/logout">
				     			<a className="nav-link">Logout</a>
				              </Link>
			              </>
				      	 	: 
				      	  <>
	                          <Link href="/login">
				     			<a className="nav-link">Login</a>
				              </Link>
					      	 
					      	  <Link href="/register">
				     			<a className="nav-link">Register</a>
				              </Link>
			              </>
				      	 }
				      	 
				      	</> 
				      		    
				    </Nav>
				 </Navbar.Collapse>
			  </Container>
		</Navbar>
   	)
} 
