//identify first the materials you will need in order to create this task. 
import {Card, Button} from 'react-bootstrap'; 

import PropTypes from 'prop-types'

const Course = ({course}) => {

//lets destructure the course prop into its properties. while describing the properties that we want take from the object.
// const { name, description, price, instructor } = course;



//lets create a new function called enroll 
function enroll() {
	console.log("thank you for enrolling to this course!")
}

   return(
		<Card>
		  <Card.Body>
		  	 <Card.Title>Course Name:</Card.Title>
		  	 <Card.Text>
		  	 	<span>Course Description:</span>
		  	 	<br />
		  	 	  
		  	 	<br />
		  	 	<span>Course Price:</span>
		  	 	<br />
		  	 	PhP 
		  	 	<br />
		  	 	<span>Course Instructor:</span>
		  	 	<br />
		  	 	 
		  	 	<br />
		  	 </Card.Text>

		  	 <Button variant="success" onClick={enroll}>Enroll</Button>
		  	 
		  </Card.Body>
		</Card>
   	)
}



export default Course; 
//lets use the prop-types to check if the components are getting the correct data.
//propTypes -> this is a method in the ES6 class-style react properties.
Course.propTypes = {
	//shape() is used to check that a property of an object conforms to a specific shape.
	propertiesNgObject: PropTypes.shape({
		name: PropTypes.string.isRequired, 
		description: PropTypes.string.isRequired, 
		price: PropTypes.number.isRequired, 
		instructor: PropTypes.string.isRequired
	})
}
//isRequired => means that the object should have all necessary/required properties.it means that including this parameter , you are assigning all objects should have the correct structure.
