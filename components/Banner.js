//practice creating components using JSX syntax
//the purpose of this component is to be the hero section of our page.

import { Jumbotron, Container, Button } from 'react-bootstrap' // -> named export

//acquire bootstrap grid system
import { Row, Col } from 'react-bootstrap'

import Link from 'next/link'

//create a function that will return the structure of my component

const Banner = ({data}) => {
	//lets declare a return scope to determine the anatomy of the element.

	//lets destructure the data prop into its properties
	const {title, content, destination, label} = data
	return (
	<Container>
		<Row>
			<Col>
				<Jumbotron>
					<h1>{title}</h1>
					<p>{content}</p>
					<Link href={destination}><a>{label}</a></Link>
				</Jumbotron>
			</Col>	
		</Row>
	</Container>

	)
}

export default Banner;