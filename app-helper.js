module.exports = {
	API_URL: process.env.NEXT_PUBLIC_API_URL,
	toJSON: (response) => response.json()
	// getAccessToken: () => localStorage.getItem('token'),
}