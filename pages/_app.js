import { useState, useEffect } from 'react'
import '../styles/globals.css'
import 'bootstrap/dist/css/bootstrap.min.css'; 
import Navbar from '../components/NavBar'; 
import { Container } from 'react-bootstrap';

//lets make tis documents as our provider component.
import { UserProvider } from '../UserContext'

function MyApp({ Component, pageProps }) {

	const [user, setUser] = useState({
		email: null,
		isAdmin: null
	})

	useEffect(() => {
		setUser({
			email: localStorage.getItem('email'),
			isAdmin: localStorage.getItem('isAdmin') === 'true'
		})
	}, [])//an optional parameter

	//lets create another effect hook for testing the setUser() functionality
	useEffect(() => {
		//to test the changes lets display the value of the user variable inside the console
		console.log(`${user.email} logged in`)
	}, [user.email])

	const unsetUser = () => {
		localStorage.clear()
		//simply reset back the user components to its default state/value
		setUser({
			email: null,
			isAdmin: null
		})
	}

  return(
  	<>
		<UserProvider value={{user, setUser, unsetUser}}>
		  	<Navbar />
		  	<Container>
		    	<Component {...pageProps} />
		  	</Container>
		</UserProvider>
  	</> 
  ) 
}

export default MyApp
