import UserContext from '../../UserContext'
import { useContext, useEffect } from 'react';
import Router from 'next/router'

export default function index () {
	const { unsetUser } = useContext(UserContext)

	//create a side effect that will allow us to clear out the local storage for us to "unmount" the current logged in user.
	useEffect(() => {
		unsetUser();
		Router.push('/login');
	}, [])
	return null
}