//identify the components that you will need to build this new element.

import { Card, Row, Col } from 'react-bootstrap';

//identify the procedures in order to build this page
const HighLights = () => {
	//describe the anatomy of the element.
	return (
		<Row>
		{/*1st card*/}
			<Col xs={12} md={4}>
				<Card className="cardHighLights">
					<Card.Body>
						<Card.Title>Learn From Home</Card.Title>
							<Card.Text>
								Lorem ipsum dolor, sit amet consectetur adipisicing elit. Temporibus repudiandae expedita distinctio, rerum quaerat, exercitationem perspiciatis aspernatur recusandae dicta, minus ipsam fuga cupiditate iusto, itaque. Illum, dolorem labore tempora excepturi?
							</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			{/*2nd card*/}
			<Col xs={12} md={4}>
				<Card className="cardHighLights">
					<Card.Body>
						<Card.Title>Study Now, Pay Later!</Card.Title>
							<Card.Text>
								Lorem ipsum dolor, sit amet consectetur adipisicing elit. Temporibus repudiandae expedita distinctio, rerum quaerat, exercitationem perspiciatis aspernatur recusandae dicta, minus ipsam fuga cupiditate iusto, itaque. Illum, dolorem labore tempora excepturi?
							</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			{/*3rd card*/}
			<Col xs={12} md={4}>
				<Card className="cardHighLights">
					<Card.Body>
						<Card.Title>Be Part of Our Community!</Card.Title>
							<Card.Text>
								Lorem ipsum dolor, sit amet consectetur adipisicing elit. Temporibus repudiandae expedita distinctio, rerum quaerat, exercitationem perspiciatis aspernatur recusandae dicta, minus ipsam fuga cupiditate iusto, itaque. Illum, dolorem labore tempora excepturi?
							</Card.Text>
					</Card.Body>
				</Card>
			</Col>

		</Row>
	)
}

export default HighLights;