import Head from 'next/head'
import Banner from '../components/Banner'
import Course from '../components/Course'
import Highlights from '../components/Highlights'
import Navbar from '../components/NavBar'
//lets integrate the particles-bg dependency.
import dynamic from 'next/dynamic'
const ParticlesBg = dynamic(() => import("particles-bg"), { ssr: false })


export default function Home() {
  //lets create the "data" object which represents the props being passed down in our banner component
  const data = {
    title: "Batch 99 Next Booking App",
    content: "Opportunities for everyone, everywhere!",
    destination: "/courses",
    label: "Enroll Now!"
  }

  return (
    <div>  
       <Banner data={data}/>
       <Highlights />  
       <ParticlesBg type="cobweb" bg={true}/>
    </div>
  )
}
